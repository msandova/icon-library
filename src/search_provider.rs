use crate::application::Action;
use crate::config::APP_ID;
use crate::models::IconsModel;
use dbus::arg;
use glib::Sender;
use gtk::prelude::*;
use search_provider::{ResultMetadata, SearchProvider as SP};
use std::collections::HashMap;
use std::sync::Arc;
use std::{cell::RefCell, rc::Rc};

#[derive(Clone)]
pub struct SearchProvider {
    search_provider: Arc<SP>,
    window: libhandy::ApplicationWindow,
    sender: Sender<Action>,
    model: Rc<IconsModel>,
}

impl SearchProvider {
    pub fn new(model: Rc<IconsModel>, window: libhandy::ApplicationWindow, sender: Sender<Action>) -> Rc<RefCell<Self>> {
        let search_provider = SP::new(APP_ID.to_string(), "/org/gnome/design/IconLibrary/SearchProvider".to_string());

        let sp = Rc::new(RefCell::new(Self {
            search_provider,
            window,
            model,
            sender,
        }));

        sp.borrow().setup_callbacks(sp.clone());
        sp
    }

    fn setup_callbacks(&self, sp: Rc<RefCell<Self>>) {
        self.search_provider.connect_activate_result(
            clone!(@strong self.window as window, @strong self.sender as sender, @strong sp => move |sp_id, _, timestamp| {
                // Show window
                window.present_with_time(timestamp);
                window.show_all();
                window.present();
                // Export the icon
                if let Some(icon) = sp.borrow().model.get_icon_byname(sp_id) {
                    send!(sender, Action::Export(icon));
                }
            }),
        );

        self.search_provider.connect_get_initial_result_set(clone!(@strong sp => move |terms| {
             sp.borrow().clone().search(terms)
        }));

        self.search_provider.connect_get_subsearch_result_set(clone!(@strong sp => move |_, terms| {
             sp.borrow().clone().search(terms)
        }));

        self.search_provider.connect_get_result_metas(clone!(@strong sp => move |sp_ids| {
            sp_ids.into_iter().map(clone!(@strong sp => move |sp_id| {
                if let Some(icon) = sp.borrow().model.get_icon_byname(&sp_id) {
                    if let Some(icon_data) = icon.get_icon_data() {
                        let mut hashmap: HashMap<String, arg::Variant<Box<dyn arg::RefArg + 'static>>> = HashMap::new();
                        hashmap.insert("id".to_string(), arg::Variant(Box::new(sp_id.to_string())));
                        hashmap.insert("name".to_string(), arg::Variant(Box::new(sp_id.to_string())));
                        hashmap.insert("description".to_string(), arg::Variant(Box::new("".to_string())));
                        hashmap.insert("icon-data".to_string(), icon_data.into());

                        return Some(ResultMetadata { hashmap });
                    }
                }
                None

            }))
            .filter(|some_icon| some_icon.is_some())
            .map(|some_icon| some_icon.unwrap())
            .collect::<Vec<ResultMetadata>>()

        }));
    }

    fn search(self, terms: Vec<&str>) -> Vec<String> {
        let icons = self.model.filter(terms);
        let mut sp_ids = icons.into_iter().map(|icon| icon.name).collect::<Vec<String>>();
        sp_ids.sort();
        sp_ids
    }
}
