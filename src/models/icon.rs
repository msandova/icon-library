use crate::config;
use dbus::arg;
use gio::prelude::*;
use gtk::prelude::*;
use serde::de;
use serde::Deserialize;
use std::path::{Path, PathBuf};
use std::str::FromStr;

#[derive(Clone, Debug, Deserialize)]
pub struct Icon {
    #[serde(deserialize_with = "icon_name")]
    pub name: String,
    #[serde(default = "not_system")]
    pub is_system: bool,
    pub tags: Vec<String>,
    pub context: String,
}

const TARGET_SVG: u32 = 0;
const TARGET_SVG_XML: u32 = 1;

pub struct IconData {
    width: i32,
    height: i32,
    rowstride: i32,
    has_alpha: bool,
    bits_per_sample: i32,
    n_channels: i32,
    data: Vec<u8>,
}

impl IconData {
    pub fn new_from_pixbuf(pixbuf: gdk_pixbuf::Pixbuf) -> Option<Self> {
        if let Some(bytes) = pixbuf.read_pixel_bytes() {
            return Some(Self {
                width: pixbuf.get_width(),
                height: pixbuf.get_height(),
                rowstride: pixbuf.get_rowstride(),
                has_alpha: pixbuf.get_has_alpha(),
                bits_per_sample: pixbuf.get_bits_per_sample(),
                n_channels: pixbuf.get_n_channels(),
                data: bytes.to_vec(),
            });
        }
        None
    }
}

impl From<IconData> for arg::Variant<Box<dyn arg::RefArg>> {
    fn from(icon_data: IconData) -> Self {
        /*
            From org.gnome.ShellSearchProvider DBus Interface documentation
            'icon-data': raw image data as (iiibiiay) - width, height, rowstride,
                        has-alpha, bits per sample, channels, data
        */
        arg::Variant(Box::new((
            icon_data.width,
            icon_data.height,
            icon_data.rowstride,
            icon_data.has_alpha,
            icon_data.bits_per_sample,
            icon_data.n_channels,
            icon_data.data,
        )))
    }
}

impl Icon {
    pub fn should_display(&self, search_str: &str) -> bool {
        let icon_name = self.name.to_lowercase();
        let icon_terms: Vec<&str> = icon_name.split('-').collect();
        let search_str = search_str.to_lowercase();
        // Check if the icon should be shown for the searched string
        let mut found_tags = self.tags.clone();
        found_tags.retain(|tag| tag.to_lowercase().contains(&search_str));

        icon_terms.contains(&search_str.as_str())
            || !found_tags.is_empty()
            || icon_name == search_str
            || icon_name.contains(&search_str.as_str())
            || self.context.to_lowercase().contains(&search_str)
    }

    pub fn derive(&self) -> anyhow::Result<()> {
        let icon_file = gio::File::new_for_path(self.get_path());
        // Push the icon into /tmp so other apps can access it for now. Not ideal :(
        let mut dest = std::env::temp_dir();
        dest.push(icon_file.get_basename().unwrap());
        let dest = gio::File::new_for_path(dest);
        self.save(&dest)?;

        let uri = dest.get_uri();
        gtk::idle_add(move || {
            if let Err(err) = gio::AppInfo::launch_default_for_uri(&uri, None::<&gio::AppLaunchContext>) {
                error!("Failed to open the project in Inkscape {}", err);
            }
            glib::Continue(false)
        });
        Ok(())
    }

    pub fn save(&self, destination: &gio::File) -> anyhow::Result<()> {
        let icon_file = gio::File::new_for_path(self.get_path());
        icon_file.copy(destination, gio::FileCopyFlags::OVERWRITE, gio::NONE_CANCELLABLE, None)?;
        Ok(())
    }

    pub fn copy(&self) {
        let display = gdk::Display::get_default().unwrap();
        let clipboard = gtk::Clipboard::get_default(&display).unwrap();

        let svg_target = gtk::TargetEntry::new("image/svg", gtk::TargetFlags::OTHER_APP, TARGET_SVG);
        let svg_xml_target = gtk::TargetEntry::new("image/svg+xml", gtk::TargetFlags::OTHER_APP, TARGET_SVG_XML);
        let targets = [svg_target, svg_xml_target];

        let icon_path = self.get_path();

        let svg_targets = targets.clone();
        clipboard.set_with_data(&targets, move |_, selection_data, target_id| {
            if [TARGET_SVG, TARGET_SVG_XML].contains(&target_id) {
                // load the svg content
                let icon_file = gio::File::new_for_path(icon_path.clone());
                let svg = icon_file.load_contents(gio::NONE_CANCELLABLE).unwrap();
                // copy the svg content to clipboard when requested
                let target = svg_targets.get(target_id as usize).unwrap().get_target();
                selection_data.set(&gdk::Atom::intern(target), 8, &svg.0);
            }
        });
    }

    pub fn copy_name(&self) {
        if let Some(display) = gdk::Display::get_default() {
            if let Some(clipboard) = gtk::Clipboard::get_default(&display) {
                clipboard.set_text(&self.name);
            }
        }
    }

    pub fn get_path(&self) -> PathBuf {
        let mut icon_path = Path::new("").to_path_buf();
        if self.is_system {
            let theme = gtk::IconTheme::get_default().unwrap();
            let icon_info = theme.lookup_icon(&self.name, -1, gtk::IconLookupFlags::FORCE_SYMBOLIC).unwrap();
            icon_path.push(icon_info.get_filename().unwrap());
        } else {
            let icon_uri: PathBuf = [config::PKGDATADIR, "icon-dev-kit", &format!("{}.svg", self.name)].iter().collect();
            icon_path.push(icon_uri);
        }
        icon_path
    }

    pub fn get_pixbuf(&self) -> Option<gdk_pixbuf::Pixbuf> {
        let theme = gtk::IconTheme::get_default()?;
        let icon_info = theme.lookup_icon(&self.name, 24, gtk::IconLookupFlags::FORCE_SYMBOLIC)?;
        let is_light = true; // For once we have FreeDesktop dark theme standard

        // Default GNOME Shell colors
        let mut fg_color = String::new();
        let warning_color = String::from("#f57900");
        let error_color = String::from("#ff8080");
        let mut success_color = String::new();
        if is_light {
            fg_color.push_str("#eeeeec");
            success_color.push_str("#33d17a");
        } else {
            fg_color.push_str("#2e3436");
            success_color.push_str("#229656");
        }

        if let Ok((pixbuf, _)) = icon_info.load_symbolic(
            &gdk::RGBA::from_str(&fg_color).unwrap(), // We are sure that RGBA can parse those colors
            Some(&gdk::RGBA::from_str(&success_color).unwrap()),
            Some(&gdk::RGBA::from_str(&warning_color).unwrap()),
            Some(&gdk::RGBA::from_str(&error_color).unwrap()),
        ) {
            return Some(pixbuf);
        }
        None
    }

    pub fn get_icon_data(&self) -> Option<IconData> {
        /*
            The IconData is used on GNOME Shell Search Provider
            The GNOME Shell Search Provider allow sending enough information to recreate a pixbuf
            for the images. See gdk_pixbuf_new_from_data
        */
        if let Some(pixbuf) = self.get_pixbuf() {
            return IconData::new_from_pixbuf(pixbuf);
        }
        None
    }
}

// Until we get https://github.com/serde-rs/serde/issues/368 fixed
fn not_system() -> bool {
    false
}

fn icon_name<'de, D>(deserializer: D) -> Result<String, D::Error>
where
    D: de::Deserializer<'de>,
{
    let icon = String::deserialize(deserializer)?;
    let icon_name = format!("{}-symbolic", icon);
    Ok(icon_name)
}
