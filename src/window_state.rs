use anyhow::Result;
use gio::prelude::SettingsExt;
use gtk::prelude::GtkWindowExt;
use gtk::SettingsExt as gtkSettingsExt;

pub fn load(window: &libhandy::ApplicationWindow, settings: &gio::Settings) {
    let width = settings.get_int("window-width");
    let height = settings.get_int("window-height");

    if width > -1 && height > -1 {
        window.resize(width, height);
    }

    let x = settings.get_int("window-x");
    let y = settings.get_int("window-y");
    let is_maximized = settings.get_boolean("is-maximized");

    if x > -1 && y > -1 {
        window.move_(x, y);
    } else if is_maximized {
        window.maximize();
    }

    // dark mode
    let is_dark_mode = settings.get_boolean("dark-mode");
    if let Some(gtk_settings) = gtk::Settings::get_default() {
        gtk_settings.set_property_gtk_application_prefer_dark_theme(is_dark_mode);
    }
}

pub fn save(window: &libhandy::ApplicationWindow, settings: &gio::Settings) -> Result<()> {
    let size = window.get_size();
    let position = window.get_position();

    settings.set_int("window-width", size.0)?;
    settings.set_int("window-height", size.1)?;
    settings.set_boolean("is-maximized", window.is_maximized())?;
    settings.set_int("window-x", position.0)?;
    settings.set_int("window-y", position.1)?;

    Ok(())
}
