use crate::config;
use crate::models::{Icon, IconsModel};
use crate::search_provider::SearchProvider;
use crate::widgets::{ExportDialog, Window};
use gio::prelude::*;
use glib::Receiver;
use gtk::prelude::*;
use std::env;
use std::path::PathBuf;
use std::{cell::RefCell, rc::Rc};

pub enum Action {
    Export(Icon),
}

pub struct Application {
    app: gtk::Application,
    window: Window,
    receiver: RefCell<Option<Receiver<Action>>>,
}

impl Application {
    pub fn new() -> Self {
        let app = gtk::Application::new(Some(config::APP_ID), Default::default()).unwrap();
        let (sender, r) = glib::MainContext::channel(glib::PRIORITY_DEFAULT);
        let receiver = RefCell::new(Some(r));

        let model = Rc::new(IconsModel::new());
        let window = Window::new(sender.clone(), model.clone());

        SearchProvider::new(model, window.widget.clone(), sender);

        let application = Self { app, window, receiver };

        application.setup_gactions();
        application.setup_signals();
        application.setup_css();
        application
    }

    fn setup_gactions(&self) {
        // Quit
        action!(
            self.app,
            "quit",
            clone!(@strong self.app as app => move |_, _| {
                app.quit();
            })
        );

        // About
        action!(
            self.app,
            "about",
            clone!(@weak self.window.widget as window => move |_, _|{
                let builder = gtk::Builder::from_resource("/org/gnome/design/IconLibrary/about_dialog.ui");
                get_widget!(builder, gtk::AboutDialog, about_dialog);
                about_dialog.set_transient_for(Some(&window));
                about_dialog.connect_response(|dialog, _| dialog.close());
                about_dialog.show();
            })
        );

        // Dark mode
        let settings = gio::Settings::new(config::APP_ID);
        if let Some(gtk_settings) = gtk::Settings::get_default() {
            settings.bind("dark-mode", &gtk_settings, "gtk-application-prefer-dark-theme", gio::SettingsBindFlags::DEFAULT);
        }
        let is_dark_mode = settings.get_boolean("dark-mode");
        stateful_action!(self.app, "dark-mode", is_dark_mode, move |action, _| {
            let state = action.get_state().unwrap();
            let action_state: bool = state.get().unwrap();
            let is_dark_mode = !action_state;
            action.set_state(&is_dark_mode.to_variant());
            if let Err(err) = settings.set_boolean("dark-mode", is_dark_mode) {
                error!("Failed to switch dark mode: {} ", err);
            }
        });
        // Accels
        self.app.set_accels_for_action("app.dark-mode", &["<primary>T"]);
        self.app.set_accels_for_action("app.about", &["<primary>comma"]);
        self.app.set_accels_for_action("app.quit", &["<primary>q"]);
        self.app.set_accels_for_action("win.show-help-overlay", &["<primary>question"]);
    }

    fn setup_signals(&self) {
        self.app.connect_startup(clone!(@weak self.window.widget as window => move |app| {
            window.set_application(Some(app));
            app.add_window(&window);
        }));
        self.app.connect_activate(clone!(@weak self.window.widget as window => move |_| {
            window.show_all();
            window.present();
        }));
    }

    fn do_action(&self, action: Action) -> glib::Continue {
        match action {
            Action::Export(icon) => {
                let export_dialog = ExportDialog::new(icon);
                export_dialog.widget.set_transient_for(Some(&self.window.widget));
                export_dialog.widget.show();
            }
        }
        glib::Continue(true)
    }

    fn setup_css(&self) {
        let p = gtk::CssProvider::new();
        gtk::CssProvider::load_from_resource(&p, "/org/gnome/design/IconLibrary/style.css");
        if let Some(screen) = gdk::Screen::get_default() {
            gtk::StyleContext::add_provider_for_screen(&screen, &p, 500);
        }
        if let Some(theme) = gtk::IconTheme::get_default() {
            let dev_kit_path: PathBuf = [config::PKGDATADIR, "icon-dev-kit"].iter().collect();
            theme.prepend_search_path(dev_kit_path);
            theme.add_resource_path("/org/gnome/design/IconLibrary/icons");
        }
    }

    pub fn run(&self, app: Rc<Self>) {
        info!("{}Icon Library({})", config::NAME_PREFIX, config::APP_ID);
        info!("Version: {} ({})", config::VERSION, config::PROFILE);
        info!("Datadir: {}", config::PKGDATADIR);

        let receiver = self.receiver.borrow_mut().take().unwrap();
        receiver.attach(None, move |action| app.do_action(action));

        let args: Vec<String> = env::args().collect();
        self.app.run(&args);
    }
}
