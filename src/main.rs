#[macro_use]
extern crate log;
#[macro_use]
extern crate glib;
#[macro_use]
extern crate serde_derive;
#[macro_use]
extern crate gtk_macros;

use gettextrs::*;
use std::rc::Rc;

mod application;
mod config;
mod models;
mod search_provider;
mod static_resources;
mod widgets;
mod window_state;

use application::Application;
use config::{GETTEXT_PACKAGE, LOCALEDIR, NAME_PREFIX};

fn main() {
    pretty_env_logger::init();

    gtk::init().expect("Failed to initalize gtk3");
    libhandy::init();

    glib::set_application_name(&format!("{}Icon Library", NAME_PREFIX));
    glib::set_prgname(Some("icon-library"));

    // Prepare i18n
    setlocale(LocaleCategory::LcAll, "");
    bindtextdomain(GETTEXT_PACKAGE, LOCALEDIR);
    textdomain(GETTEXT_PACKAGE);

    static_resources::init().expect("Failed to initialize the gresource");

    let app = Rc::new(Application::new());
    app.run(app.clone());
}
