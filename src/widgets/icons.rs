use crate::application::Action;
use crate::models::{Icon, IconsContext};
use glib::Sender;
use gtk::prelude::*;
use std::cell::RefCell;
use std::collections::HashMap;
use std::rc::Rc;

pub struct IconsView {
    pub widget: gtk::Stack,
    builder: gtk::Builder,

    icons_contexts: Vec<RefCell<IconsViewContext>>,
    sender: glib::Sender<Action>,
}

impl IconsView {
    pub fn new(sender: glib::Sender<Action>) -> Self {
        let builder = gtk::Builder::from_resource("/org/gnome/design/IconLibrary/icons_view.ui");
        get_widget!(builder, gtk::Stack, icons_view);

        Self {
            widget: icons_view,
            builder,
            icons_contexts: Vec::new(),
            sender,
        }
    }

    pub fn filter(&self, search_str: &str) {
        let mut total_results = 0;
        let mut total_system_results = 0;
        for icons_context_view in self.icons_contexts.iter() {
            icons_context_view.borrow_mut().filter(search_str);
            total_results += icons_context_view.borrow().n_last_search_results;

            if icons_context_view.borrow().icons_context.is_system {
                total_system_results += icons_context_view.borrow().n_last_search_results;
            }
        }

        get_widget!(self.builder, gtk::Box, system_help_container);
        if total_system_results != 0 {
            system_help_container.show();
        } else {
            system_help_container.hide();
        }

        if total_results != 0 {
            self.widget.set_visible_child_name("results");
        } else {
            self.widget.set_visible_child_name("no-results");
        }
    }

    pub fn add_context(&mut self, context: IconsContext) {
        let icons_view_context = IconsViewContext::new(context.clone(), self.sender.clone());
        if context.is_system {
            get_widget!(self.builder, gtk::Box, system_icons_container);
            system_icons_container.pack_start(&icons_view_context.widget, false, true, 12);
        } else {
            get_widget!(self.builder, gtk::Box, shipped_icons_container);
            shipped_icons_container.pack_start(&icons_view_context.widget, false, true, 12);
        }
        self.icons_contexts.push(RefCell::new(icons_view_context));
    }
}

struct IconsViewContext {
    pub widget: gtk::Box,
    pub icons_context: IconsContext,
    icons_container: gtk::FlowBox,
    icons_views: Rc<RefCell<HashMap<gtk::FlowBoxChild, Icon>>>,
    n_last_search_results: u32,
    sender: glib::Sender<Action>,
}

impl IconsViewContext {
    pub fn new(icons_context: IconsContext, sender: glib::Sender<Action>) -> IconsViewContext {
        let widget = gtk::Box::new(gtk::Orientation::Vertical, 6);
        let icons_container = gtk::FlowBox::new();
        let icons_views = Rc::new(RefCell::new(HashMap::new()));
        let n_last_search_results: u32 = 0;
        let mut icons_view_context = IconsViewContext {
            widget,
            icons_context,
            icons_container,
            icons_views,
            n_last_search_results,
            sender,
        };
        icons_view_context.init();
        icons_view_context
    }

    fn init(&mut self) {
        self.icons_container.set_row_spacing(18);
        self.icons_container.set_column_spacing(18);
        self.icons_container.set_homogeneous(true);
        self.icons_container.set_max_children_per_line(14);
        self.icons_container.set_min_children_per_line(6);
        self.icons_container.set_halign(gtk::Align::Fill);
        self.icons_container.set_valign(gtk::Align::Start);
        self.icons_container.set_selection_mode(gtk::SelectionMode::None);

        let context_label = gtk::Label::new(Some(&self.icons_context.context));
        context_label.set_halign(gtk::Align::Start);
        context_label.get_style_context().add_class("title-4");
        context_label.set_property_margin(6);
        self.widget.add(&context_label);

        gtk::idle_add(clone!(@weak self.icons_container as icons_container,
            @weak self.icons_views as icons_views , @strong self.sender as sender,
            @strong self.icons_context as icons_context => @default-return glib::Continue(false) ,
        move || {
            for icon in icons_context.icons.iter() {
                let icon_view = IconView::new(sender.clone(), icon.clone());
                icons_container.add(&icon_view.widget);
                icon_view.widget.show_all();
                icons_views.borrow_mut().insert(icon_view.widget, icon.clone());
            }
            glib::Continue(false)
        }));
        self.icons_container.show();
        self.widget.add(&self.icons_container);
    }
    pub fn filter(&mut self, search_str: &str) {
        self.n_last_search_results = 0;

        let search_str = search_str.to_string();

        self.icons_container.set_filter_func(Some(Box::new(
            clone!(@weak self.icons_views as icons_views => @default-return false, move |child| {
                let icon = icons_views.borrow();
                let icon = icon.get(child);
                match icon {
                    Some(icon) => icon.should_display(&search_str),
                    None => {
                        error!("Search: couldn't find the icon");
                        false
                    }
                }
            }),
        )));
        for child in self.icons_container.get_children() {
            if child.get_child_visible() {
                self.n_last_search_results += 1;
            }
        }
        self.widget.set_visible(self.n_last_search_results != 0);
    }
}

struct IconView {
    pub widget: gtk::FlowBoxChild,
    icon: Icon,
    sender: Sender<Action>,
}

impl IconView {
    pub fn new(sender: Sender<Action>, icon: Icon) -> IconView {
        let widget = gtk::FlowBoxChild::new();
        let icon_view = IconView { widget, icon, sender };
        icon_view.init();
        icon_view
    }

    fn init(&self) {
        // FlowBoxChild -> EventBox -> GtkImage
        self.widget.set_halign(gtk::Align::Center);
        self.widget.set_valign(gtk::Align::Center);
        self.widget.set_tooltip_text(Some(&self.icon.name));

        let event_box = gtk::EventBox::new();

        event_box.connect_button_press_event(clone!(@strong self.sender as sender, @strong self.icon as icon => move |_, _| {
            send!(sender, Action::Export(icon.clone()));
            gtk::Inhibit(false)
        }));
        event_box.show();
        self.widget.add(&event_box);

        let icon_img = gtk::Image::from_icon_name(Some(&self.icon.name), gtk::IconSize::Dnd);
        icon_img.get_style_context().add_class("icon");
        event_box.add(&icon_img);
    }
}
