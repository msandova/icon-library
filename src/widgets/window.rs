use crate::application::Action;
use crate::config::{APP_ID, PROFILE};
use crate::models::IconsModel;
use crate::widgets::IconsView;
use crate::window_state;
use gtk::prelude::*;
use std::rc::Rc;

pub struct Window {
    pub widget: libhandy::ApplicationWindow,
    builder: gtk::Builder,
    sender: glib::Sender<Action>,
    pub model: Rc<IconsModel>,
}

impl Window {
    pub fn new(sender: glib::Sender<Action>, model: Rc<IconsModel>) -> Self {
        let settings = gio::Settings::new(APP_ID);
        let builder = gtk::Builder::from_resource("/org/gnome/design/IconLibrary/window.ui");
        get_widget!(builder, libhandy::ApplicationWindow, window);

        let window_widget = Window {
            widget: window,
            builder,
            sender,
            model,
        };
        window_widget.init(settings);
        window_widget
    }

    pub fn init(&self, settings: gio::Settings) {
        if PROFILE == "Devel" {
            self.widget.get_style_context().add_class("devel");
        }
        get_widget!(self.builder, gtk::Image, dark_mode_image);
        if let Some(gtk_settings) = gtk::Settings::get_default() {
            gtk_settings.connect_property_gtk_application_prefer_dark_theme_notify(move |settings| {
                if !settings.get_property_gtk_application_prefer_dark_theme() {
                    dark_mode_image.set_from_icon_name(Some("dark-mode-symbolic"), gtk::IconSize::Button);
                } else {
                    dark_mode_image.set_from_icon_name(Some("light-mode-symbolic"), gtk::IconSize::Button);
                }
            });
        }

        // load latest window state
        window_state::load(&self.widget, &settings);

        // save window state on delete event
        self.widget.connect_delete_event(move |window, _| {
            if let Err(err) = window_state::save(&window, &settings) {
                error!("Failed to save window state {}", err);
            }
            Inhibit(false)
        });

        let builder = gtk::Builder::from_resource("/org/gnome/design/IconLibrary/shortcuts.ui");
        get_widget!(builder, gtk::ShortcutsWindow, shortcuts);
        self.widget.set_help_overlay(Some(&shortcuts));

        // Init the widgets
        let mut icons_view = IconsView::new(self.sender.clone());
        for (_, icons_context) in self.model.system_icons.borrow().iter() {
            icons_view.add_context(icons_context.clone());
        }
        for (_, icons_context) in self.model.shipped_icons.borrow().iter() {
            icons_view.add_context(icons_context.clone());
        }
        get_widget!(self.builder, gtk::Box, container);
        container.add(&icons_view.widget);

        // Search
        get_widget!(self.builder, gtk::SearchEntry, search_entry);
        search_entry.connect_changed(move |entry| {
            let search_text = entry.get_text();
            icons_view.filter(&search_text);
        });
        search_entry.connect_stop_search(move |entry| {
            entry.set_text("");
        });
        self.widget.connect_key_press_event(move |_, _| {
            search_entry.grab_focus_without_selecting();
            gtk::Inhibit(false)
        });
    }
}
