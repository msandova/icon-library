pub static GRESOURCE_EXAMPLE: &str = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>
<gresources>
  <gresource prefix=\"/org/gnome/design/IconLibrary/\">
    <file preprocess=\"xml-stripblanks\">icons/start-here-symbolic.svg</file>
  </gresource>
</gresources>";

// Python
pub static PYTHON_MESON_EXAMPLE: &str = "gnome = import('gnome')
resources = gnome.compile_resources(
  'resources',
  'resources.gresource.xml',
  gresource_bundle: true,
  source_dir: meson.current_build_dir(),
  install: true,
  install_dir: get_option('datadir') / meson.project_name(),
)";
pub static PYTHON_EXAMPLE: &str = "import os
from gi.repository import Gio

if __name__ == \"__main__\":
    resource = Gio.resource_load(os.path.join('@PKGDATA_DIR@', 'resources.gresource'))
    Gio.Resource._register(resource)
";

// Rust
pub static RUST_MESON_EXAMPLE: &str = "gnome = import('gnome')
resources = gnome.compile_resources(
  'resources',
  'resources.gresource.xml',
  gresource_bundle: true,
  source_dir: meson.current_build_dir()
)
# include_bytes! only takes a string literal
resources_conf = configuration_data()
resources_conf.set_quoted('RESOURCEFILE', resources.full_path())
static_resources_output_file = configure_file(
  input: 'static_resources.rs.in',
  output: 'static_resources.rs',
  configuration: resources_conf
)

run_command(
  'cp',
  static_resources_output_file,
  meson.current_source_dir(),
  check: true
)";

pub static RUST_STATIC_EXAMPLE: &str = "use gio::{resources_register, Resource};
use glib::{Error, Bytes};

pub(crate) fn init() -> Result<(), Error> {
    // load the gresource binary at build time and include/link it into the final
    // binary.
    let res_bytes = include_bytes!(@RESOURCEFILE@);

    // Create Resource it will live as long the value lives.
    let gbytes = Bytes::from_static(res_bytes.as_ref());
    let resource = Resource::new_from_data(&gbytes)?;

    // Register the resource so it won't be dropped and will continue to live in
    // memory.
    resources_register(&resource);

    Ok(())
}";
pub static RUST_MAIN_EXAMPLE: &str = "mod static_resources;
fn main () {
    static_resources::init().expect(\"Failed to find the gresource\");
}";

// Vala
pub static VALA_MESON_EXAMPLE: &str = "gnome = import('gnome')
resource_files = files('gnome-clocks.gresource.xml')
resources = gnome.compile_resources('resources',
  'resources.gresource.xml',
  c_name: 'resources'
)
# Include the resource in the binary
executable(meson.project_name(),
  [
    'main.vala',
    resources,
  ],
  install: true
)";

// JS
pub static JS_MESON_EXAMPLE: &str = "gnome = import('gnome')

gnome.compile_resources(
  'org.gnome.design.IconLibrary.data',
  'resources.data.gresource.xml',
  gresource_bundle: true,
  install_dir: get_option('datadir') / meson.project_name(),
  install: true
)";

pub static JS_EXAMPLE: &str = "imports.package.init({ name: \"org.gnome.design.IconLibrary\" });
imports.package.run(imports.app.main);";

// C
pub static C_MESON_EXAMPLE: &str = "gnome = import('gnome')
resources = gnome.compile_resources('resources',
  'resources.gresource.xml',
  source_dir: '.',
  c_name: 'resources'
)
executable(
  meson.project_name(),
  [
    'main.c',
    resources
  ],
  install: true
)";
