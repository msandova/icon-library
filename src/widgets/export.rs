use crate::config;
use crate::models::Icon;
use gettextrs::gettext;
use gio::prelude::SettingsExt as GioSettingsExt;
use gio::prelude::*;
use gtk::prelude::*;
use gtk::prelude::{SettingsExt, TextBufferExt};
use gtk::Inhibit;
use libhandy::{ActionRowExt, DeckExt, HeaderBarExt};
use sourceview4::prelude::*;
use std::path::Path;

use super::examples::*;

pub struct ExportDialog {
    pub widget: gtk::Window,
    builder: gtk::Builder,
    icon: Icon,
}

impl ExportDialog {
    pub fn new(icon: Icon) -> ExportDialog {
        let builder = gtk::Builder::from_resource("/org/gnome/design/IconLibrary/export_dialog.ui");
        get_widget!(builder, gtk::Window, export_dialog);

        let export_dialog_widget = ExportDialog {
            widget: export_dialog,
            builder,
            icon,
        };
        export_dialog_widget.init();
        export_dialog_widget.setup_actions();
        export_dialog_widget
    }

    fn create_buffer(lang: &sourceview4::Language, content: &str, view: sourceview4::View) -> sourceview4::Buffer {
        let buffer = sourceview4::Buffer::with_language(lang);
        view.set_buffer(Some(&buffer));
        buffer.set_highlight_syntax(true);
        buffer.set_highlight_matching_brackets(false);
        TextBufferExt::set_text(&buffer, content);
        buffer
    }

    fn init(&self) {
        let settings = gio::Settings::new(config::APP_ID);
        get_widget!(self.builder, gtk::Image, icon_size_16);
        get_widget!(self.builder, gtk::Image, icon_size_32);
        get_widget!(self.builder, gtk::Image, icon_size_64);

        get_widget!(self.builder, gtk::Stack, stack1);
        settings.bind("doc-page", &stack1, "visible-child-name", gio::SettingsBindFlags::DEFAULT);

        icon_size_16.set_from_icon_name(Some(&self.icon.name), gtk::IconSize::Button);
        icon_size_32.set_from_icon_name(Some(&self.icon.name), gtk::IconSize::Dnd);
        icon_size_64.set_from_icon_name(Some(&self.icon.name), gtk::IconSize::Dialog);

        get_widget!(self.builder, gtk::Label, tags_label);
        tags_label.set_text(&self.icon.tags.join(", "));

        get_widget!(self.builder, libhandy::HeaderBar, headerbar);
        headerbar.set_title(Some(&self.icon.name.replace("-symbolic", "")));

        get_widget!(self.builder, gtk::Label, include_label);
        include_label.connect_activate_link(clone!(@strong self.builder as builder => @default-return Inhibit(false), move |_, _| {
            get_widget!(builder, libhandy::Deck, deck);
            get_widget!(builder, gtk::Stack, stack);
            stack.set_visible_child_name("in-app");
            deck.navigate(libhandy::NavigationDirection::Forward);
            Inhibit(false)
        }));

        let mut scheme_name = "solarized-light";
        if let Some(gtk_settings) = gtk::Settings::get_default() {
            if gtk_settings.get_property_gtk_application_prefer_dark_theme() {
                scheme_name = "solarized-dark";
            }
        }
        let scheme = sourceview4::StyleSchemeManager::get_default()
            .and_then(|scm| scm.get_scheme(scheme_name))
            .unwrap();

        get_widget!(self.builder, libhandy::ActionRow, as_system_row);
        as_system_row.set_visible(self.icon.is_system);
        let language_manager = sourceview4::LanguageManager::get_default().unwrap();
        let xml_lang = language_manager.get_language("xml").unwrap();
        let py_lang = language_manager.get_language("python").unwrap();
        let meson_lang = language_manager.get_language("meson").unwrap();
        let rust_lang = language_manager.get_language("rust").unwrap();
        let js_lang = language_manager.get_language("js").unwrap();

        get_widget!(self.builder, sourceview4::View, resource_sourceview);
        let resource = format!("<file preprocess=\"xml-stripblanks\">icons/{}.svg</file>", self.icon.name);
        ExportDialog::create_buffer(&xml_lang, &resource, resource_sourceview).set_style_scheme(Some(&scheme));

        // Python example
        get_widget!(self.builder, sourceview4::View, python_meson_sourceview);
        ExportDialog::create_buffer(&meson_lang, &PYTHON_MESON_EXAMPLE, python_meson_sourceview).set_style_scheme(Some(&scheme));
        get_widget!(self.builder, sourceview4::View, python_sourceview);
        ExportDialog::create_buffer(&py_lang, &PYTHON_EXAMPLE, python_sourceview).set_style_scheme(Some(&scheme));

        // Rust example
        get_widget!(self.builder, sourceview4::View, rust_meson_sourceview);
        ExportDialog::create_buffer(&meson_lang, &RUST_MESON_EXAMPLE, rust_meson_sourceview).set_style_scheme(Some(&scheme));
        get_widget!(self.builder, sourceview4::View, rust_static_sourceview);
        ExportDialog::create_buffer(&rust_lang, &RUST_STATIC_EXAMPLE, rust_static_sourceview).set_style_scheme(Some(&scheme));

        get_widget!(self.builder, sourceview4::View, rust_main_sourceview);
        ExportDialog::create_buffer(&rust_lang, &RUST_MAIN_EXAMPLE, rust_main_sourceview).set_style_scheme(Some(&scheme));

        // Vala example
        get_widget!(self.builder, sourceview4::View, vala_meson_sourceview);
        ExportDialog::create_buffer(&meson_lang, &VALA_MESON_EXAMPLE, vala_meson_sourceview).set_style_scheme(Some(&scheme));

        // JS example
        get_widget!(self.builder, sourceview4::View, js_meson_sourceview);
        ExportDialog::create_buffer(&meson_lang, &JS_MESON_EXAMPLE, js_meson_sourceview).set_style_scheme(Some(&scheme));
        get_widget!(self.builder, sourceview4::View, js_sourceview);
        ExportDialog::create_buffer(&js_lang, &JS_EXAMPLE, js_sourceview).set_style_scheme(Some(&scheme));

        // C example
        get_widget!(self.builder, sourceview4::View, c_meson_sourceview);
        ExportDialog::create_buffer(&meson_lang, &C_MESON_EXAMPLE, c_meson_sourceview).set_style_scheme(Some(&scheme));
    }

    fn setup_actions(&self) {
        // setup actions
        let actions = gio::SimpleActionGroup::new();
        self.widget.insert_action_group("export", Some(&actions));

        // Copy Icon Name action
        action!(
            actions,
            "copy-icon-name",
            clone!(@strong self.icon as icon => move |_, _| {
                icon.copy_name();
            })
        );
        // Switch view to: Include the icon in an App
        get_widget!(self.builder, libhandy::ActionRow, in_app_row);
        in_app_row.connect_activated(clone!(@strong self.builder as builder => move |_| {
            get_widget!(builder, libhandy::Deck, deck);
            get_widget!(builder, gtk::Stack, stack);
            stack.set_visible_child_name("in-app");
            deck.navigate(libhandy::NavigationDirection::Forward);
        }));
        // Switch the view to Icon details
        action!(
            actions,
            "details",
            clone!(@strong self.builder as builder => move |_, _| {
                get_widget!(builder, libhandy::Deck, deck);
                deck.navigate(libhandy::NavigationDirection::Back);
            })
        );
        // Switch view to in-platform related stuff
        get_widget!(self.builder, libhandy::ActionRow, as_system_row);
        as_system_row.connect_activated(clone!(@strong self.builder as builder => move |_| {
            get_widget!(builder, libhandy::Deck, deck);
            get_widget!(builder, gtk::Stack, stack);
            stack.set_visible_child_name("in-platform");
            deck.navigate(libhandy::NavigationDirection::Forward);
        }));
        // Save GResource sample
        action!(
            actions,
            "save-resource",
            clone!(@weak self.widget as window => move |_, _| {
                let export_dialog = gtk::FileChooserNative::new(
                    Some(&gettext("Export GResource example file")),
                    Some(&window),
                    gtk::FileChooserAction::Save,
                    Some(&gettext("Export")),
                    Some(&gettext("Cancel")),
                );

                let svg_filter = gtk::FileFilter::new();
                svg_filter.set_name(Some(&gettext("XML Document")));
                svg_filter.add_mime_type("application/xml");
                export_dialog.add_filter(&svg_filter);

                export_dialog.set_current_name(Path::new("resources.gresource.xml"));

                export_dialog.connect_response(move |dialog, response| {
                    if response == gtk::ResponseType::Accept {
                        let destination = dialog.get_file().unwrap();

                        if let Err(err) = destination.replace_contents(GRESOURCE_EXAMPLE.as_bytes(), None,
                                                    false, gio::FileCreateFlags::NONE, gio::NONE_CANCELLABLE) {
                            error!("Failed to save gresource example {}", err);
                        }
                    }
                    dialog.destroy();
                });
                export_dialog.run();
            })
        );

        // Save As action
        action!(
            actions,
            "save-as",
            clone!(@strong self.icon as icon, @weak self.widget as window => move |_, _| {
                let export_dialog = gtk::FileChooserNative::new(
                    Some(&gettext("Export a symbolic icon")),
                    Some(&window),
                    gtk::FileChooserAction::Save,
                    Some(&gettext("Export")),
                    Some(&gettext("Cancel")),
                );

                let svg_filter = gtk::FileFilter::new();
                svg_filter.set_name(Some(&gettext("SVG images")));
                svg_filter.add_mime_type("image/svg+xml");
                export_dialog.add_filter(&svg_filter);

                let file_name = format!("{}.svg", icon.name);
                export_dialog.set_current_name(Path::new(&file_name));

                export_dialog.connect_response(clone!(@strong icon => move |dialog, response| {
                    if response == gtk::ResponseType::Accept {
                        let destination = dialog.get_file().unwrap();
                        if let Err(err) = icon.save(&destination) {
                            error!("Failed to export the icon {}", err);
                        }
                    }
                    dialog.destroy();
                }));
                export_dialog.run();
            })
        );

        // Derive icon
        action!(
            actions,
            "derive",
            clone!(@strong self.icon as icon => move |_, _| {
                if let Err(err) = icon.derive() {
                    error!("Failed to derive the icon {}", err);
                }
            })
        );

        // Copy icon to clipboard action: copy-clipboard
        action!(
            actions,
            "copy-clipboard",
            clone!(@strong self.icon as icon => move |_, _| {
                icon.copy();
            })
        );

        // Quit
        self.widget.connect_delete_event(|w, _| {
            w.close();
            gtk::Inhibit(false)
        });
        self.widget.connect_key_press_event(|w, k| {
            if k.get_keyval() == gdk::keys::constants::Escape {
                w.close();
            }
            gtk::Inhibit(false)
        });
    }
}
