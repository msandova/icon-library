# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE’S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# Thibault Martin <mail@thibaultmart.in>, 2020.
# Sylvestris <sylvestris@tutanota.com>, 2020.
#
msgid ""
msgstr ""
"Project-Id-Version: 0\n"
"Report-Msgid-Bugs-To: https://gitlab.gnome.org/World/design/icon-library/"
"issues\n"
"POT-Creation-Date: 2020-08-25 21:19+0000\n"
"PO-Revision-Date: 2020-08-27 12:13+0200\n"
"Last-Translator: Sylvestris <sylvestris@tutanota.com>\n"
"Language-Team: French <gnomefr@traduc.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1)\n"
"X-Generator: Gtranslator 3.36.0\n"

#: data/org.gnome.design.IconLibrary.desktop.in.in:3
#: data/org.gnome.design.IconLibrary.metainfo.xml.in.in:7
#: data/resources/ui/about_dialog.ui.in:9 data/resources/ui/window.ui.in:54
msgid "Icon Library"
msgstr "Bibliothèque d’icônes"

#: data/org.gnome.design.IconLibrary.desktop.in.in:4
msgid "Find the right icon to use on your GNOME application"
msgstr "Trouver la bonne icône pour son application GNOME"

#. Translators: Search terms to find this application. Do NOT translate or localize the semicolons! The list MUST also end with a semicolon!
#: data/org.gnome.design.IconLibrary.desktop.in.in:10
msgid "Icon;Library;GNOME;GTK;"
msgstr "Icône;Bibliothèque;GNOME;GTK;"

#: data/org.gnome.design.IconLibrary.gschema.xml.in:6
msgid "Width of the last opened window"
msgstr "Largeur de la dernière fenêtre ouverte"

#: data/org.gnome.design.IconLibrary.gschema.xml.in:10
msgid "Height of the last opened window"
msgstr "Hauteur de la dernière fenêtre ouverte"

#: data/org.gnome.design.IconLibrary.gschema.xml.in:14
msgid "Left position of the last opened window"
msgstr "Position gauche de la dernière fenêtre ouverte"

#: data/org.gnome.design.IconLibrary.gschema.xml.in:18
msgid "Top position of the last opened window"
msgstr "Position supérieure de la dernière fenêtre ouverte"

#: data/org.gnome.design.IconLibrary.gschema.xml.in:22
msgid "Maximized state of the last opened window"
msgstr "État maximisé de la dernière fenêtre ouverte"

#: data/org.gnome.design.IconLibrary.gschema.xml.in:26
msgid "Enable or disable dark mode"
msgstr "Mode sombre"

#: data/org.gnome.design.IconLibrary.gschema.xml.in:30
msgid "The default doc-page"
msgstr "La page de documentation par défaut"

#: data/org.gnome.design.IconLibrary.metainfo.xml.in.in:8
msgid "Symbolic icons for your apps"
msgstr "Icônes symboliques pour vos applications"

#: data/org.gnome.design.IconLibrary.metainfo.xml.in.in:10
msgid "Find the right icon to use on your GNOME application."
msgstr "Trouver la bonne icône pour son application GNOME"

#: data/org.gnome.design.IconLibrary.metainfo.xml.in.in:15
msgid "Main Window"
msgstr "Fenêtre principale"

#: data/org.gnome.design.IconLibrary.metainfo.xml.in.in:72
msgid "Bilal Elmoussaoui"
msgstr "Bilal Elmoussaoui"

#: data/resources/ui/about_dialog.ui.in:13
msgid "translator-credits"
msgstr "Thibault Martin <mail@thibaultmart.in>"

#: data/resources/ui/export_dialog.ui:29
msgid "Save as…"
msgstr "Enregistrer sous…"

#: data/resources/ui/export_dialog.ui:45
msgid "Copy Name"
msgstr "Copier le nom de l’icône"

#: data/resources/ui/export_dialog.ui:266
msgid "Use in a Mockup"
msgstr "Utiliser dans une maquette"

#: data/resources/ui/export_dialog.ui:269
msgid "Copy to Clipboard"
msgstr "Copier dans le presse-papier"

#: data/resources/ui/export_dialog.ui:288
#: data/resources/ui/export_dialog.ui:343
msgid "Include in an App"
msgstr "Inclure dans une application"

#: data/resources/ui/export_dialog.ui:303
#: data/resources/ui/export_dialog.ui:1193
msgid "Use as a System Icon"
msgstr "Utiliser comme icône système"

#: data/resources/ui/export_dialog.ui:392
msgid ""
"In order to use a custom icon in your app, it needs to be shipped as a "
"GResource."
msgstr ""
"Pour utiliser une icône personnalisée dans votre application, elle doit être "
"intégrée en tant que GResource."

#: data/resources/ui/export_dialog.ui:409
msgid ""
"The first step is saving the file in your project directory (e.g. in <b>/"
"data/icons</b>)."
msgstr ""
"La première étape est d’enregistrer le fichier dans le dossier de votre "
"projet (ex. : dans <b>/data/icons</b>)."

#: data/resources/ui/export_dialog.ui:424
msgid "Save SVG to…"
msgstr "Enregistrer le fichier SVG sous…"

#: data/resources/ui/export_dialog.ui:446
msgid ""
"Then you need to include the file in your resource XML file, with a line "
"similar to this:"
msgstr ""
"Puis vous devez inclure le fichier dans votre fichier ressource XML, avec "
"une ligne similaire à ceci :"

#: data/resources/ui/export_dialog.ui:499
msgid ""
"You should be able to use the icon by setting <i>icon-name</i> property of "
"<i>Gtk.Image</i>"
msgstr ""
"Vous devriez pouvoir utiliser l’icône en définissant la propriété <i>icon-"
"name</i> de <i>Gtk.Image</i>"

#: data/resources/ui/export_dialog.ui:519
msgid "No Resource File?"
msgstr "Pas de fichier ressource ?"

#: data/resources/ui/export_dialog.ui:538
msgid ""
"If you don't have a resource file in your project yet, you can start from "
"this example file."
msgstr ""
"Si vous n’avez pas encore de fichier ressource dans votre projet, vous "
"pouvez commencer par ce fichier d’exemple."

#: data/resources/ui/export_dialog.ui:552
msgid "Save Resource File to…"
msgstr "Enregistrer le fichier ressource vers…"

#: data/resources/ui/export_dialog.ui:574
msgid ""
"<a href='https://developer.gnome.org/gio/stable/GResource.html'>Read more on "
"GResources</a>"
msgstr ""
"<a href='https://developer.gnome.org/gio/stable/GResource.html'>En savoir "
"plus sur les GResources</a>"

#: data/resources/ui/export_dialog.ui:593
msgid "Include the GResource in your app"
msgstr "Inclure la GResource dans votre app"

#: data/resources/ui/export_dialog.ui:641
#: data/resources/ui/export_dialog.ui:705
msgid ""
"In order for this to work you need to include it in your meson.build, like "
"so:"
msgstr ""
"Pour que ceci fonctionne, vous devez l’inclure dans votre meson.build, comme "
"ceci :"

#: data/resources/ui/export_dialog.ui:757
#: data/resources/ui/export_dialog.ui:1046
msgid "And then load it in your main file"
msgstr "Puis chargez-le dans votre fichier principal"

#: data/resources/ui/export_dialog.ui:822
msgid ""
"In order for this to work you need to create a bundle out of the "
"<b>GResource</b> file and install it in <b>$prefix/$datadir/$app-name</b>. "
"Here's how you would do it using Meson"
msgstr ""
"Pour que ceci fonctionne, vous devez d’abord créer un lot à partir de la "
"<b>GResource</b> puis l’installer dans <b>$prefix/$datadir/$app-name</b>. "
"Voici comment faire en utilisant Meson"

#: data/resources/ui/export_dialog.ui:875
msgid "And then load the bundle in your main file"
msgstr "Puis chargez le lot dans votre fichier principal"

#: data/resources/ui/export_dialog.ui:940
msgid ""
"In order for this to work you need to first create a bundle out of the "
"<b>GResource</b> and then include it using a pre-configured file "
"<i>static_resources.rs.in</i>."
msgstr ""
"Pour que ceci fonctionne, vous devez d’abord créer un lot à partir de la "
"<b>GResource</b> puis l’inclure en utilisant un fichier <i>static_resources."
"rs.in</i> pré-configuré."

#: data/resources/ui/export_dialog.ui:993
msgid ""
"The <i>static_resources.rs.in</i> file includes the resource and registers "
"it."
msgstr ""
"Le fichier <i>static_resources.rs.in</i> inclut la ressource et la "
"répertorie."

#: data/resources/ui/export_dialog.ui:1112
msgid ""
"In order for this to work you need to include the <b>GResource</b> in the "
"binary."
msgstr ""
"Pour que ceci fonctionne, vous devez inclure la <b>GResource</b> dans "
"l’exécutable."

#: data/resources/ui/export_dialog.ui:1236
msgid ""
"This is a system icon, which means it comes pre-installed as part of the "
"platform."
msgstr ""
"Ceci est une icône système, ce qui veut dire qu’elle est installée en tant "
"que partie du système d’exploitation."

#: data/resources/ui/export_dialog.ui:1253
msgid ""
"Note that system icons can change over time, so if you are using this icon "
"because of what it looks like rather than its semantic meaning it would be "
"better to <a href='in-app'>include it with your app</a>."
msgstr ""
"Notez que les icônes système peuvent changer avec le temps, ainsi si vous "
"utilisez cette icône pour son apparence plutôt que sa sémantique, il est "
"préférable de <a href='in-app'>l’inclure dans votre application</a>."

#: data/resources/ui/export_dialog.ui:1268
msgid "Copy Icon Name"
msgstr "Copier le nom de l’icône"

#: data/resources/ui/icons_view.ui:56
msgid "Pre-Installed System Icons"
msgstr "Icônes système pré-installés"

#: data/resources/ui/icons_view.ui:74
msgid ""
"These icons come with the operating system, which means you have no control "
"over them and they could change at any time. Only use them if you're sure "
"you are using them in the correct context (e.g. three lines for a primary "
"menu)."
msgstr ""
"Ces icônes font partie du système d’exploitation, ce qui implique que vous "
"n’avez pas de contrôle dessus et qu’ils peuvent changer à tout moment. Ne "
"les utilisez que si vous êtes sûrs de le faire dans le bon contexte (ex. : "
"trois lignes pour un menu primaire)."

#: data/resources/ui/icons_view.ui:150
msgid "No results found"
msgstr "Aucun résultat"

#: data/resources/ui/shortcuts.ui:13
msgctxt "shortcut window"
msgid "General"
msgstr "Général"

#: data/resources/ui/shortcuts.ui:17
msgctxt "shortcut window"
msgid "Show Shortcuts"
msgstr "Montrer les raccourcis"

#: data/resources/ui/shortcuts.ui:24
msgctxt "shortcut window"
msgid "Dark Mode"
msgstr "Mode sombre"

#: data/resources/ui/shortcuts.ui:31
msgctxt "shortcut window"
msgid "Quit"
msgstr "Quitter"

#: data/resources/ui/window.ui.in:21
msgid "_Keyboard Shortcuts"
msgstr "_Raccourcis clavier"

#: data/resources/ui/window.ui.in:35
msgid "About Icon Library"
msgstr "À propos de Bibliothèque d’icônes"

#: data/resources/ui/window.ui.in:78
msgid "Search for icons by name, category or tag"
msgstr "Chercher des icônes par nom, catégorie ou mot-clé"

#: data/resources/ui/window.ui.in:91
msgid "Toggle Dark Mode"
msgstr "Basculer le mode sombre"

#: src/widgets/export.rs:170
msgid "Export GResource example file"
msgstr "Exporter le fichier d’exemple GResource"

#: src/widgets/export.rs:173 src/widgets/export.rs:208
msgid "Export"
msgstr "Exporter"

#: src/widgets/export.rs:174 src/widgets/export.rs:209
msgid "Cancel"
msgstr "Annuler"

#: src/widgets/export.rs:178
msgid "XML Document"
msgstr "Document XML"

#: src/widgets/export.rs:205
msgid "Export a symbolic icon"
msgstr "Exporter une icône symbolique"

#: src/widgets/export.rs:213
msgid "SVG images"
msgstr "Images SVG"

#~ msgid "@name-prefix@Icon Library"
#~ msgstr "@name-prefix@Bibliothèque d’icônes"

#~ msgid "This is a system icon. It does not need to be shipped with apps."
#~ msgstr ""
#~ "Il s’agit d’une icône système. Elle n’a pas besoin d’être déployée avec "
#~ "les applications."
